# Discontinued Branch

> The code in this branch ('_download-through-content_') was written in an attempt to re-design the download process that is scheduled from within __download.js__ by utilizing `fetch` mechanics. However, this attempt fails at a detail issue as outlined in __content.js__ with currently no prospect of a fix.